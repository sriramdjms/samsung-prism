from bs4 import BeautifulSoup # BeautifulSoup is in bs4 package
import requests
import pandas as pd
from pandas import DataFrame
from sklearn.feature_extraction.text import TfidfVectorizer
import re

#NDTV URL
URL = 'https://www.ndtv.com/india-news/coronavirus-live-updates-special-flights-between-iran-india-for-evacuation-2190721'
content = requests.get(URL)
soup = BeautifulSoup(content.text, 'html.parser')
rows = soup.find_all('p')
documentA=""
for row in rows:
    clean_text=""
    clean_text=clean_text.replace('\\',' ')
    clean_text= re.sub(r"""
                   [,.;@#?"!&$":-]+  # Accept one or more copies of punctuation
                   \ *           # plus zero or more copies of a space,
                   """,
                   " ",          # and replace it with a single space
                   row.get_text(), flags=re.VERBOSE)
    documentA=documentA+clean_text
rows1 = soup.find_all('h1')
documentB=""
for row in rows1:
    clean_text=""
    clean_text=clean_text.replace('\\',' ')
    clean_text= re.sub(r"""
                   [,.;@#?"!&$":-]+  # Accept one or more copies of punctuation
                   \ *           # plus zero or more copies of a space,
                   """,
                   " ",          # and replace it with a single space
                   row.get_text(), flags=re.VERBOSE)
    documentB=documentB+clean_text
NDTV=documentA+documentB
print("extracting from NDTV")
#print("NDTV---------",NDTV)

#thehindu URL
URL = 'https://www.thehindu.com/news/national/covid-19-updates-march-4-2020/article30979516.ece'
content = requests.get(URL)
soup = BeautifulSoup(content.text, 'html.parser')
rows = soup.find_all('p')
documentA=""
for row in rows:
    clean_text=""
    clean_text=clean_text.replace('\\',' ')
    clean_text= re.sub(r"""
                   [,.;@#?"!&$":-]+  # Accept one or more copies of punctuation
                   \ *           # plus zero or more copies of a space,
                   """,
                   " ",          # and replace it with a single space
                   row.get_text(), flags=re.VERBOSE)
    documentA=documentA+clean_text
rows1 = soup.find_all('h1')
documentB=""
for row in rows1:
    clean_text=""
    clean_text=clean_text.replace('\\',' ')
    clean_text= re.sub(r"""
                   [,.;@#?"!&$":-]+  # Accept one or more copies of punctuation
                   \ *           # plus zero or more copies of a space,
                   """,
                   " ",          # and replace it with a single space
                   row.get_text(), flags=re.VERBOSE)
    documentB=documentB+clean_text
thehindu=documentA+documentB
print("extracting from THE HINDU")
#print("THE HINDU---------",thehindu)

#indiatoday URL
URL = 'https://www.indiatoday.in/world/story/coronavirus-outbreak-news-live-updates-india-china-iran-delhi-toll-infection-cases-who-1652916-2020-03-06'
content = requests.get(URL)
soup = BeautifulSoup(content.text, 'html.parser')
rows = soup.find_all('p')
documentA=""
for row in rows:
    clean_text=""
    clean_text=clean_text.replace('\\',' ')
    clean_text= re.sub(r"""
                   [,.;@#?"!&$":-]+  # Accept one or more copies of punctuation
                   \ *           # plus zero or more copies of a space,
                   """,
                   " ",          # and replace it with a single space
                   row.get_text(), flags=re.VERBOSE)
    documentA=documentA+clean_text
rows1 = soup.find_all('h1')
documentB=""
for row in rows1:
    clean_text=""
    clean_text=clean_text.replace('\\',' ')
    clean_text= re.sub(r"""
                   [,.;@#?"!&$":-]+  # Accept one or more copies of punctuation
                   \ *           # plus zero or more copies of a space,
                   """,
                   " ",          # and replace it with a single space
                   row.get_text(), flags=re.VERBOSE)
    documentB=documentB+clean_text
indiatoday=documentA+documentB
print("extracting from INDIA TODAY")
#print("INDIA TODAY----",indiatoday)

bagOfWordsA = NDTV.split(' ')
bagOfWordsB = thehindu.split(' ')
bagOfWordsC = indiatoday.split(' ')

uniqueWordsinitial = set(bagOfWordsA).union(set(bagOfWordsB))
uniqueWords=set(uniqueWordsinitial).union(set(bagOfWordsC))

"""def validateString(s):
    letter_flag = False
    number_flag = False
    for i in s:
        if i.isalpha():
            letter_flag = True
        if i.isdigit():
            number_flag = True
    return letter_flag and number_flag"""


numOfWordsA = dict.fromkeys(uniqueWords, 0)
for word in bagOfWordsA:
    """checkstr=validateString(word)
    if checkstr==False:"""
    """try:
        word=word.lower()
    except KeyError:
        word=word"""
    numOfWordsA[word] += 1
numOfWordsB = dict.fromkeys(uniqueWords, 0)
for word in bagOfWordsB:
    """checkstr=validateString(word)
    if checkstr==False:"""
    """try:
        word=word.lower()
    except KeyError:
        word=word"""
    numOfWordsB[word] += 1
numOfWordsC = dict.fromkeys(uniqueWords, 0)
for word in bagOfWordsC:
    """checkstr=validateString(word)
    if checkstr==False:"""
    """try:
        word=word.lower()
    except KeyError:
        word=word"""
    numOfWordsC[word] += 1

import nltk
nltk.download('stopwords')

from nltk.corpus import stopwords
stopwords.words('english')

def computeTF(wordDict, bagOfWords):
    tfDict = {}
    bagOfWordsCount = len(bagOfWords)
    for word, count in wordDict.items():
        tfDict[word] = count / float(bagOfWordsCount)
    return tfDict

tfA = computeTF(numOfWordsA, bagOfWordsA)
tfB = computeTF(numOfWordsB, bagOfWordsB)
tfC = computeTF(numOfWordsC, bagOfWordsC)

def computeIDF(documents):
    import math
    N = len(documents)

    idfDict = dict.fromkeys(documents[0].keys(), 0)
    for document in documents:
        for word, val in document.items():
            if val > 0:
                idfDict[word] += 1

    for word, val in idfDict.items():
        idfDict[word] = math.log(N / float(val))
    return idfDict

idfs = computeIDF([numOfWordsA, numOfWordsB, numOfWordsC])

def computeTFIDF(tfBagOfWords, idfs):
    tfidf = {}
    for word, val in tfBagOfWords.items():
        tfidf[word] = val * idfs[word]
    return tfidf

tfidfA = computeTFIDF(tfA, idfs)
tfidfB = computeTFIDF(tfB, idfs)
tfidfC = computeTFIDF(tfC, idfs)


df = pd.DataFrame([tfidfA, tfidfB,tfidfC])

print(df)
df.to_excel("output.xlsx")
